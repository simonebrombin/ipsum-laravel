<?php

namespace Ipsumlab\Core\Traits;
use DB;

trait SubQuery
{

    public static function getSub($attr, $foreign, $key = null){
        $t = new self();

        $select = $t->getTable(). '.'.$attr;

        if($attr instanceof \Illuminate\Database\Query\Expression){
            $select = $attr;
        }

        return self::select($select)
            ->whereRaw(DB::getTablePrefix() . $t->getTable().'.'. (!empty($key) ? $key : $t->getKeyName()).' = '. $foreign)
            ->limit(1)
            ->getQuery();
    }
}
