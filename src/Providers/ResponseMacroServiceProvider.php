<?php

namespace Ipsumlab\Core\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('datatable', function ($data, $recordsTotal, $recordsFiltered, $draw) {
            return Response::json([
                'data' => $data,
                "errors" => false,
                "draw" => $draw,
                "recordsTotal"  => $recordsTotal,
                "recordsFiltered" => $recordsFiltered,
            ]);
        });

        Response::macro('success', function ($data) {
            return Response::json([
                 'errors'  => false,
                 'data' => $data,
                ]);
        });

        Response::macro('error', function ($message, $status = 400) {
            return Response::json([
            'message'         => $status.' error',
            'errors'          => [
            'message'         => [$message],
            ],
            'status_code'     => $status,
            ], $status);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
